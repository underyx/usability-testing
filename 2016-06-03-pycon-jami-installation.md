Subject: coala installation
Version: N/A

- user googles coala, gets sidetracked by Nicholas Cage's face photoshopped onto
  a coala
- finds our github as 1st result, website as 3rd result, clicks on github link
- user is offended at ASCII art in README because it looks like the coala is
  giving her the finger
- user clicks on the asciinema link, start watching the demo, says 'whoa' a few
  seconds in (post-testing note: it's because the demo was too fast for her to
  follow)
- wondering whether there's some narration she should be hearing, tries looking
  for captions
- gives up, reads About section
- 'shouldn't coala be capitalized?'
- 'shouldn't the name be italicized everywhere?'
- 'languages supported by coala-bears link' is next to 'various bears supported
  by coala' link, user is confused about the difference
- User tries to find Meteor, Mongo, express.js, AngularJS in list of supported
  languages. Thinks some generic bears seem interesting, and thinks the
  javascript bears might work for her.
- Expects to be able to click the supported language list items.
- 'all languages prototype' page is confusing, why does it exist?
- user notes that the pages list in the github wiki is ordered weirdly
- user is trying to click the checkboxes in the bear list to pick and choose
  which ones she wants, doesn't work, feels weird about it
- trying to figure out which bears would be useful for her, reading
  'Available Bears', notes lack of descriptions, unsure if for example the
  SpellCheckBear would work for javascript or just some languages
- user is wondering if maybe they need to fork it to use it
- user finds the description and link at the top of github, clicks link
- no CTA visible above the fold on website
- setup details (known to normal people as 'get it') is found
- user is still not sure what coala is for, not clear on what to do with
  `sudo pip3 install coala-bears`
- user is on windows so sudo does not work
- pip3 is not recognized either
- user tried to install on python 2, so installation failed
- pip's error code 4 thing is pretty scary and confusing
- user knows she has to type python3, wonders if there are separate pips
- added C:\Python34\Scripts to $PATH following a stack overflow post but still
  does not work
- ran `easy_install pip` based on another SO post
- user spends a few minutes researching, restarts cmd, still doesn't work
- somehow even regular `pip` broke now
- we give up
